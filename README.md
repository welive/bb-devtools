# README #


### What is this repository for? ###

The purpose of this repository is to store the development tools which demonstrate to developers the usage of building block and their integration.


### How do I get set up? ###

* project contains subfolder bb-client and templates.
* bb-clients folder holds wrapper client libraries for building blocks.
* templates folder holds template web applications demonstrating the usage and integration of the bb-clients.
* read the developer-guide.txt in clients.
* in order to install and run the app read install.txt file inside the template projects.