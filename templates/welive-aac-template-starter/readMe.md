START AS SPRING BOOT APPLICATION.
--------------------------------
1. Build the project 
	mvn clean package
	
2. Run the command
	java -Dserver.port=8080 -Dserver.contextPath=/starter starter.jar

where starter.jar file is located inside target folder after maven build.


START AS WEBAPPLICATION.
-----------------------

1. Change the <packaging> to war in pom.xml file. 

2. Run mvn clean package and copy the starter.war file inside target folder to tomcat servlet container. We tested it on apache tomcate 7 and 8.


Note: in order to change the port, it is required to change the configuration of oauth client accordingly(redirectUrl, singlelogoutUrl) in particular. 