<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<groupId>it.smartcommunitylab</groupId>
	<artifactId>welive-aac-template-starter</artifactId>
	<version>1.0</version>
	<packaging>jar</packaging>

	<properties>
		<spring-boot.version>1.4.0.RELEASE</spring-boot.version>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<springfox-swagger-ui.version>2.4.0</springfox-swagger-ui.version>
		<snakeyaml.version>1.14</snakeyaml.version>
		<maven-license-plugin.version>1.8.0</maven-license-plugin.version>
	</properties>

	<parent>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-parent</artifactId>
		<version>1.4.0.RELEASE</version>
		<relativePath /> <!-- lookup parent from repository -->
	</parent>

	<dependencies>
		<!-- CLIENTS -->
		<dependency>
			<groupId>it.smartcommunitylab</groupId>
			<artifactId>welive.aac.client</artifactId>
			<version>1.0</version>
		</dependency>
		<dependency>
			<groupId>it.smartcommunitylab</groupId>
			<artifactId>welive.logging.client</artifactId>
			<version>1.0</version>
		</dependency>
		<!-- JSON -->
		<dependency>
			<groupId>org.codehaus.jackson</groupId>
			<artifactId>jackson-mapper-lgpl</artifactId>
			<version>1.9.4</version>
			<type>jar</type>
			<scope>compile</scope>
		</dependency>
		<dependency>
			<groupId>cglib</groupId>
			<artifactId>cglib</artifactId>
			<version>2.2.2</version>
		</dependency>
		<!-- JUNIT -->
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<version>4.11</version>
			<scope>test</scope>
		</dependency>
		<!-- Swagger -->
		<dependency>
			<groupId>io.springfox</groupId>
			<artifactId>springfox-swagger2</artifactId>
			<version>${springfox-swagger-ui.version}</version>
		</dependency>
		<dependency>
			<groupId>io.springfox</groupId>
			<artifactId>springfox-swagger-ui</artifactId>
			<version>${springfox-swagger-ui.version}</version>
		</dependency>
		<!-- WADL -->
		<dependency>
			<groupId>com.autentia.web.rest</groupId>
			<artifactId>spring-wadl-generator</artifactId>
			<version>1.1-SNAPSHOT</version>
		</dependency>
		<!-- USDL -->
		<dependency>
			<groupId>it.smartcommunitylab</groupId>
			<artifactId>welive.bb.usdl.generator</artifactId>
			<version>1.0</version>
		</dependency>
		<!-- SPRING BOOT -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot</artifactId>
			<version>${spring-boot.version}</version>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-actuator</artifactId>
			<version>${spring-boot.version}</version>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-security</artifactId>
			<version>${spring-boot.version}</version>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
			<version>${spring-boot.version}</version>
		</dependency>
		<dependency>
			<groupId>org.springframework.security.oauth</groupId>
			<artifactId>spring-security-oauth2</artifactId>
			<version>2.0.10.RELEASE</version>
		</dependency>
		<dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-tomcat</artifactId>
            <scope>provided</scope>
        </dependency>
		<dependency>
			<groupId>org.webjars</groupId>
			<artifactId>angularjs</artifactId>
			<version>1.4.3</version>
		</dependency>
		<dependency>
			<groupId>org.webjars</groupId>
			<artifactId>jquery</artifactId>
			<version>2.1.1</version>
		</dependency>
		<dependency>
			<groupId>org.webjars</groupId>
			<artifactId>bootstrap</artifactId>
			<version>3.2.0</version>
		</dependency>
		<dependency>
			<groupId>org.webjars</groupId>
			<artifactId>webjars-locator</artifactId>
			<version>0.32</version>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-test</artifactId>
			<version>${spring-boot.version}</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-configuration-processor</artifactId>
			<version>${spring-boot.version}</version>
			<optional>true</optional>
		</dependency>
		<!-- YAML -->
		<dependency>
			<groupId>org.yaml</groupId>
			<artifactId>snakeyaml</artifactId>
			<version>${snakeyaml.version}</version>
		</dependency>
	</dependencies>

	<build>
		<plugins>
			<plugin>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-maven-plugin</artifactId>
			</plugin>
		</plugins>
		<finalName>starter</finalName>
	</build>

	<repositories>
		<repository>
			<id>SmartCommunity-releases</id>
			<url>http://repository.smartcommunitylab.it/content/repositories/releases</url>
		</repository>
		<repository>
			<id>com.springsource.repository.bundles.release</id>
			<name>SpringSource Enterprise Bundle Repository - SpringSource
					Bundle Releases</name>
			<url>http://repository.springsource.com/maven/bundles/release</url>
		</repository>
		<repository>
			<id>com.springsource.repository.bundles.external</id>
			<name>SpringSource Enterprise Bundle Repository - External Bundle
					Releases</name>
			<url>http://repository.springsource.com/maven/bundles/external</url>
		</repository>
	</repositories>

	<profiles>
		<profile>
			<id>license</id>
			<activation>
				<property>
					<name>run-license</name>
					<value>true</value>
				</property>
			</activation>
			<build>
				<plugins>
					<plugin>
						<inherited>false</inherited>
						<groupId>com.mycila.maven-license-plugin</groupId>
						<artifactId>maven-license-plugin</artifactId>
						<version>${maven-license-plugin.version}</version>
						<configuration>
							<header>${basedir}/license/license.txt</header>
							<failIfMissing>true</failIfMissing>
							<aggregate>true</aggregate>
							<properties>
								<organisation>Smart Community Lab</organisation>
								<year>2017</year>
							</properties>
							<includes>
								<include>**/*.java</include>
							</includes>
						</configuration>
					</plugin>
				</plugins>
			</build>
		</profile>
	</profiles>

</project>
