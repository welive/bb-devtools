/**
 * Copyright 2017 Smart Community Lab
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.smartcommunitylab.aac.template.starter.app;

import java.security.Principal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.oauth2.resource.ResourceServerProperties;
import org.springframework.boot.autoconfigure.security.oauth2.resource.UserInfoTokenServices;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.core.env.Environment;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.filter.OAuth2ClientAuthenticationProcessingFilter;
import org.springframework.security.oauth2.client.filter.OAuth2ClientContextFilter;
import org.springframework.security.oauth2.client.token.grant.code.AuthorizationCodeResourceDetails;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.filter.CompositeFilter;
import org.springframework.web.servlet.ModelAndView;

import com.google.common.collect.Lists;

import it.smartcommunitylab.aac.template.model.Response;
import it.smartcommunitylab.aac.template.utils.TemplateUtils;
import it.smartcommunitylab.logging.LoggingService;
import it.smartcommunitylab.logging.model.LogMsg;
import it.smartcommunitylab.logging.model.Pagination;
import it.smartcommunitylab.welive.aac.client.exception.WeliveException;
import it.smartcommunitylab.welive.aac.client.model.BasicProfile;
import it.smartcommunitylab.welive.aac.client.model.ClientModel;
import it.smartcommunitylab.welive.aac.client.model.TokenData;
import it.smartcommunitylab.welive.aac.client.service.AuthService;
import it.smartcommunitylab.welive.aac.client.service.ProfileService;
import it.smartcommunitylab.welive.aac.client.service.RemoteTokenServices;
import it.smartcommunitylab.welive.aac.client.service.ResourceService;
import springfox.documentation.annotations.ApiIgnore;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@RestController
@EnableOAuth2Client
@EnableSwagger2
@Order(6)
public class StarterApp extends WebSecurityConfigurerAdapter {

	@Autowired
	public Environment env;
	@Autowired
	OAuth2ClientContext oauth2ClientContext;
	/** user bearer token. **/
	private static String userToken;
	/** client token. **/
	private static String clientToken;
	/** app. **/
	private static final String APPID = "testApp";
	/** log schema for app. **/
	private static final String SCHEMATYPE = "starterApp";
	/** logging service. **/
	private LoggingService loggingService;
	/** scope protected url patterns. **/
	private static final String[] SCOPE_PROTECTED_URL_PATTERNS = new String[] { "/api/**" };

	@ApiIgnore
	@RequestMapping({ "/user", "/me" })
	public Map<String, Object> user(Principal principal) {
		Map<String, Object> map = new LinkedHashMap<String, Object>();
		Authentication a = SecurityContextHolder.getContext().getAuthentication();
		if (a.getDetails() != null && a.getDetails() instanceof OAuth2AuthenticationDetails) {
			OAuth2AuthenticationDetails details = (OAuth2AuthenticationDetails) a.getDetails();
			userToken = details.getTokenValue();
			System.err.println(env.getProperty("logging.endpoint"));
			try {
				TokenData data = authService().generateClientToken();
				clientToken = data.getAccess_token();
				ClientModel clientModel = resourceService().getClientSpec(clientToken);
				// initialize logging.
				loggingService = LoggingService.logClient(env.getProperty("logging.endpoint"), "Bearer " + clientToken);
				String jsonSchema = TemplateUtils.readFile(SCHEMATYPE + "-schema.json");
				System.out.println(
						"Logging Schema Updated: " + loggingService.updateSchema(APPID, jsonSchema, SCHEMATYPE));
				// log msg.
				pushLog("LOGIN");
				map.put("name", principal.getName());
				map.put("appName", clientModel.getName());
				map.put("clientId", clientModel.getClientId());
				map.put("Idps", clientModel.getIdentityProviders().toString());
				map.put("logs", getLogs());
			} catch (WeliveException e) {
				LogMsg exception = new LogMsg();
				exception.setTimestamp(System.currentTimeMillis());
				Map<String, Object> attrs = new HashMap<String, Object>();
				attrs.put("error", e.getMessage());
				exception.setCustomAttributes(attrs);
				List<LogMsg> exceptions = new ArrayList<LogMsg>();
				exceptions.add(exception);
				map.put("name", principal.getName());
				map.put("logs", exceptions);
			}
		}

		return map;
	}

	@ApiIgnore
	@RequestMapping({ "/updateLog" })
	private Object updateLog() {
		List<LogMsg> logs = new ArrayList<LogMsg>();
		try {
			logs = getLogs();
		} catch (WeliveException e) {
			LogMsg exception = new LogMsg();
			exception.setTimestamp(System.currentTimeMillis());
			Map<String, Object> attrs = new HashMap<String, Object>();
			attrs.put("error", e.getMessage() + " reading Logs from server");
			exception.setCustomAttributes(attrs);
			logs.add(exception);
		}
		return logs;
	}

	@ApiIgnore
	@RequestMapping({ "/readAccessToken" })
	private Map<String, Object> readAccessToken() {
		Map<String, Object> token = new HashMap<String, Object>();
		token.put("auth", "Bearer " + clientToken) ;
		return token;

	}

	@ApiIgnore
	@RequestMapping({ "/logout" })
	public ModelAndView logout(HttpServletRequest req, HttpServletResponse res) {
		return new ModelAndView("redirect:" + env.getProperty("oauthServer.serverConfig.logoutUrl") + "?service="
				+ env.getProperty("aac.resource.serviceId"));//().http://localhost:5057/login/aac");
	}

	@ApiIgnore
	@RequestMapping("/oauth/client/singleLogout")
	public ModelAndView logout(HttpServletRequest req, HttpServletResponse res,
			@RequestParam(value = "RelayState") String RelayState,
			@RequestParam(value = "SAMLRequest") String SAMLRequest) {
		req.getSession().invalidate();
		return new ModelAndView(
				"redirect:" + env.getProperty("oauthServer.serverConfig.logoutUrl") + "?RelayState=" + RelayState);
	}

	@PreAuthorize("#oauth2.hasScope('profile.basicprofile.me')")
	@RequestMapping(method = RequestMethod.GET, value = "/api/echo")
	public @ResponseBody Response<String> echo(@RequestHeader(value = "Authorization") String token)
			throws WeliveException {

		// fetch basic profile.
		BasicProfile profile = profileService().getBasicProfile(userToken);
		pushLog("BASIC PROFILE");
		return new Response<String>("Hi " + profile.getName() + "!");
	}

	private void pushLog(String action) {
		LogMsg payload = new LogMsg();
		payload.setAppId(APPID);
		Map<String, Object> attrs = new HashMap<String, Object>();
		attrs.put("pilot", "Trento");
		attrs.put("action", action);
		attrs.put("origin", "Template Starter App");
		payload.setCustomAttributes(attrs);
		payload.setType(SCHEMATYPE);
		payload.setMsg("TestSchema");
		loggingService.pushLog(APPID, payload);
	}

	private List<LogMsg> getLogs() throws WeliveException {
		try {
			Pagination logPage = loggingService.readLogs(APPID, null, null, null, null, null, 10, 0);
			if (logPage != null) {
				return Lists.reverse(logPage.getData());
			}
			throw new WeliveException("cannot read logs from server.");
		} catch (Exception e) {
			throw new WeliveException(e.getMessage());
		}

	}

	/** CONFIGURATIONS. **/

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		// @formatter:off
		http.antMatcher("/**").authorizeRequests()
				.antMatchers("/", "/login**", "/oauth/client/singleLogout", "/webjars/**", "/swagger-ui.html",
						"/swagger-resources")
				.permitAll().anyRequest().authenticated().and().exceptionHandling()
				.authenticationEntryPoint(new LoginUrlAuthenticationEntryPoint("/")).and().logout()
				.logoutSuccessUrl("/").permitAll().and().csrf()
				.csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse()).and()
				.addFilterBefore(ssoFilter(), BasicAuthenticationFilter.class);
		// @formatter:on

		// ignore api.
		http.authorizeRequests().antMatchers("/service/*").authenticated().anyRequest().permitAll().and().httpBasic()
				.and().csrf().disable();
	}

	@Configuration
	@EnableResourceServer
	protected static class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {

		@Autowired
		public Environment env;

		/**REMOTE TOKEN **/
		@Bean
		RemoteTokenServices remoteTokenServices() {
			RemoteTokenServices rts = new RemoteTokenServices();
			rts.setClientId(env.getProperty("aac.client.clientId"));
			rts.setClientSecret(env.getProperty("aac.client.clientSecret"));
			rts.setCheckTokenEndpointUrl(
					env.getProperty("oauthServer.serverConfig.endpoint") + "/aac/eauth/check_token");
			return rts;
		}

		@Override
		public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
			/** its a trick to avoid resourceId check. **/
			resources.resourceId(null);
			resources.tokenServices(remoteTokenServices());

		}

		/**REMOTE TOKEN **/

		@Override
		public void configure(HttpSecurity http) throws Exception {
			// @formatter:off
			http.antMatcher("/me").authorizeRequests().anyRequest().authenticated();
			// @formatter:on
			http.requestMatchers().antMatchers(SCOPE_PROTECTED_URL_PATTERNS).and().authorizeRequests().anyRequest()
					.access("#oauth2.hasScope('profile.basicprofile.me')");

		}
	}

	@Bean
	public Docket newsApi() {
		return new Docket(DocumentationType.SWAGGER_2).select()
				.apis(RequestHandlerSelectors.basePackage("it.smartcommunitylab.aac.template.starter.app")).build()
				.apiInfo(apiInfo());
	}

	private ApiInfo apiInfo() {
		return new ApiInfoBuilder().title("WeLive Starter Template REST API")
				.description(
						"This page contains an interactive representation of the Welive Starter Template project's API using Swagger.")
				.build();
	}

	@Bean
	public FilterRegistrationBean oauth2ClientFilterRegistration(OAuth2ClientContextFilter filter) {
		FilterRegistrationBean registration = new FilterRegistrationBean();
		registration.setFilter(filter);
		registration.setOrder(-100);
		return registration;
	}

	@Bean
	@ConfigurationProperties("aac")
	public ClientResources aac() {
		return new ClientResources();
	}

	private Filter ssoFilter() {
		CompositeFilter filter = new CompositeFilter();
		List<Filter> filters = new ArrayList<Filter>();
		filters.add(ssoFilter(aac(), "/login/aac"));
		filter.setFilters(filters);
		return filter;
	}

	private Filter ssoFilter(ClientResources client, String path) {
		OAuth2ClientAuthenticationProcessingFilter filter = new OAuth2ClientAuthenticationProcessingFilter(path);
		OAuth2RestTemplate template = new OAuth2RestTemplate(client.getClient(), oauth2ClientContext);
		filter.setRestTemplate(template);
		filter.setTokenServices(
				new UserInfoTokenServices(client.getResource().getUserInfoUri(), client.getClient().getClientId()));
		return filter;
	}

	@Bean
	public ProfileService profileService() {
		ProfileService profileService = new ProfileService(
				env.getProperty("oauthServer.serverConfig.endpoint") + "/aac");
		return profileService;
	}

	@Bean
	public ResourceService resourceService() {
		ResourceService resourceService = new ResourceService(
				env.getProperty("oauthServer.serverConfig.endpoint") + "/aac");
		return resourceService;
	}

	@Bean
	AuthService authService() {
		AuthService authService = new AuthService(env.getProperty("oauthServer.serverConfig.endpoint") + "/aac",
				aac().getClient().getClientId(), aac().getClient().getClientSecret());
		return authService;
	}

	public static void main(String[] args) {
		SpringApplication.run(StarterApp.class, args);
	}

}

class ClientResources {

	@NestedConfigurationProperty
	private AuthorizationCodeResourceDetails client = new AuthorizationCodeResourceDetails();

	@NestedConfigurationProperty
	private ResourceServerProperties resource = new ResourceServerProperties();

	public AuthorizationCodeResourceDetails getClient() {
		return client;
	}

	public ResourceServerProperties getResource() {
		return resource;
	}

}
