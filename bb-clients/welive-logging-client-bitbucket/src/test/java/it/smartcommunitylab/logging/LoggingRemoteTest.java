/**
 * Copyright 2017 Smart Community Lab
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.smartcommunitylab.logging;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.github.tomakehurst.wiremock.junit.WireMockRule;

import it.smartcommunitylab.logging.model.LogMsg;

public class LoggingRemoteTest {

	@Rule
	public WireMockRule wireMockRule = new WireMockRule(8888);

	private LoggingService loggingClient = LoggingService.logClient("https://test.welive.eu/dev/api/",
			"Bearer e22ed6d4-f86d-4ad1-9e74-88fd9cc4e6b5");
	// this token is generated after adding testApp resource to client logging  permission with testApp
	private static final String APPID = "testApp";
	private static final String SCHEMATYPE = "testSchema";

//	@Test
	public void updateSchema() {
		String jsonSchema = readFile("test-schema.json");
		Boolean status = loggingClient.updateSchema(APPID, jsonSchema, SCHEMATYPE);
		Assert.assertTrue(status);

	}

//	@Test
	public void log() {
		LogMsg payload = new LogMsg();
		payload.setAppId(APPID);
		Map<String, Object> attrs = new HashMap<String, Object>();
		attrs.put("pilot", "Trento");
		attrs.put("action", "LOGIN");
		payload.setCustomAttributes(attrs);
		payload.setType(SCHEMATYPE);
		payload.setMsg("TestSchema");
		Assert.assertTrue(loggingClient.pushLog(APPID, payload));
	}

//	@Test
	public void read() throws JsonProcessingException {
		Assert.assertEquals(2,
				loggingClient.readLogs(APPID, null, null, null, null, null, 2, null).getData().size());

	}

	private String readFile(String name) {
		String result = "";
		try {
			BufferedReader br = new BufferedReader(
					new InputStreamReader(Test.class.getClassLoader().getResourceAsStream(name)));
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();
			while (line != null) {
				sb.append(line);
				line = br.readLine();
			}
			result = sb.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
}
