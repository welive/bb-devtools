/**
 * Copyright 2017 Smart Community Lab
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.smartcommunitylab.usdl.generator.model;

import java.util.List;

public class BuildingBlock {
	/** namespace <welive:>. **/
	private String namespace;
	/** title. **/
	private String title;
	/** created. **/
	private String created;
	/** abstract. **/
	private String abstractText;
	/** description. **/
	private String description;
	/** type. **/
	private String type;
	/** pilot **/
	private String pilot;
	/** webpage. **/
	private String webpage;
	/** tags. **/
	private List<String> tags;
	/** url. **/
	private String url;
	/** uses. ?? **/
	private List<String> uses;
	/** business roles -> hasBusinessRole  (1:n). **/
	private List<BusinessRole> businessRole;
	/** legal condition -> hasLegalCondition. **/
	private LegalCondition license;
	/** interaction points. **/
	private List<InteractionPoint> interactionPoint;
	/** security measures. **/
	private SecurityMeasure security;
	/** service offerings. **/
	private List<String> serviceOffering;

	public BuildingBlock() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getNamespace() {
		return namespace;
	}

	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCreated() {
		return created;
	}

	public void setCreated(String created) {
		this.created = created;
	}

	public String getAbstractText() {
		return abstractText;
	}

	public void setAbstractText(String abstractText) {
		this.abstractText = abstractText;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPilot() {
		return pilot;
	}

	public void setPilot(String pilot) {
		this.pilot = pilot;
	}

	public String getWebpage() {
		return webpage;
	}

	public void setWebpage(String webpage) {
		this.webpage = webpage;
	}

	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public List<String> getUses() {
		return uses;
	}

	public void setUses(List<String> uses) {
		this.uses = uses;
	}

	public List<BusinessRole> getBusinessRole() {
		return businessRole;
	}

	public void setBusinessRole(List<BusinessRole> businessRole) {
		this.businessRole = businessRole;
	}

	public LegalCondition getLicense() {
		return license;
	}

	public void setLicense(LegalCondition license) {
		this.license = license;
	}

	public List<InteractionPoint> getInteractionPoint() {
		return interactionPoint;
	}

	public void setInteractionPoint(List<InteractionPoint> interactionPoint) {
		this.interactionPoint = interactionPoint;
	}

	public SecurityMeasure getSecurity() {
		return security;
	}

	public void setSecurity(SecurityMeasure security) {
		this.security = security;
	}

	public List<String> getServiceOffering() {
		return serviceOffering;
	}

	public void setServiceOffering(List<String> serviceOffering) {
		this.serviceOffering = serviceOffering;
	}

}
