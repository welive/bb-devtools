/**
 * Copyright 2017 Smart Community Lab
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.smartcommunitylab.usdl.generator;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.yaml.snakeyaml.Yaml;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.vocabulary.DCTerms;

import it.smartcommunitylab.usdl.generator.model.Artifact;
import it.smartcommunitylab.usdl.generator.model.BuildingBlock;
import it.smartcommunitylab.usdl.generator.model.BusinessRole;
import it.smartcommunitylab.usdl.generator.model.InteractionPoint;
import it.smartcommunitylab.usdl.generator.model.LegalCondition;
import it.smartcommunitylab.usdl.generator.model.Permission;
import it.smartcommunitylab.usdl.generator.schema.ExtWeliveCore;
import it.smartcommunitylab.usdl.generator.schema.Foaf;
import it.smartcommunitylab.usdl.generator.schema.Tags;
import it.smartcommunitylab.usdl.generator.schema.WeliveCore;
import it.smartcommunitylab.usdl.generator.schema.WeliveSecurity;

public class USDLService {

	public Artifact artifact;
	public Yaml yaml;

	private static final String OS_NS = "http://platform.smartcommunitylab.it/openservice/services#";
	private static final String SCHEMA_NS = "http://schema.org/";

	private static final Map<String, String> welivePrefixes = new HashMap<String, String>();

	// extra tags
	private static final Property schemaUrlProp = ModelFactory.createDefaultModel().createProperty(SCHEMA_NS + "url");
	private static final DateFormat DATE_FORMATTER = new SimpleDateFormat("dd/MM/yyyy");

	public USDLService(File f) {
		try {
			InputStream in = new FileInputStream(f); //new File("src/main/resources/" + routerName + ".yml"));
			yaml = new Yaml();
			artifact = yaml.loadAs(in, Artifact.class);
			// initialize prefix.
			init();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	}

	public USDLService(InputStream is) {
		yaml = new Yaml();
		try {
			artifact = yaml.loadAs(is, Artifact.class);
			// initialize prefix.
			init();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Artifact getArtifact() {
		return artifact;
	}

	public static void main(String args[]) throws ParseException {
		File f = new File("src/test/resources/artifact-reference.yml");
		USDLService usdlService = new USDLService(f);
		System.err.println(usdlService.generateBB());
	}

	private void init() {
		welivePrefixes.put("welive-core", WeliveCore.NS);
		welivePrefixes.put("dc", DCTerms.NS);
		welivePrefixes.put("foaf", Foaf.NS);
		welivePrefixes.put("welive-sec", WeliveSecurity.NS);
		welivePrefixes.put("tags", Tags.NS);
		welivePrefixes.put("schema", SCHEMA_NS);
	}

	public String generateBB() {

		BuildingBlock s = artifact.getBuildingblock();
		Model m = ModelFactory.createDefaultModel();
		m.setNsPrefixes(welivePrefixes);
		Resource buildingBlock = m.createResource(OS_NS + s.getTitle().replaceAll(" ", "-"), WeliveCore.BuildingBlock);
		if (s.getTitle() != null)
			buildingBlock.addProperty(DCTerms.title, s.getTitle());
		if (s.getDescription() != null)
			buildingBlock.addProperty(DCTerms.description, s.getDescription());
		if (s.getAbstractText() != null)
			buildingBlock.addProperty(DCTerms.abstract_, s.getAbstractText());
		if (s.getPilot() != null)
			buildingBlock.addProperty(WeliveCore.pilot, s.getPilot());
		if (s.getCreated() != null) {
			try {
				Date d = DATE_FORMATTER.parse(s.getCreated());
				buildingBlock.addProperty(DCTerms.created, DATE_FORMATTER.format(d));
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}

		buildingBlock.addProperty(WeliveCore.type, ExtWeliveCore.WebService);
		buildingBlock.addProperty(DCTerms.type, ExtWeliveCore.WebService);

		if (s.getWebpage() != null)
			buildingBlock.addProperty(Foaf.page, s.getWebpage() + "service/spec");

		// tags
		for (String t : s.getTags()) {
			buildingBlock.addProperty(Tags.tag, t);
		}

		if (!s.getBusinessRole().isEmpty()) {
			// identify roles.
			List<BusinessRole> owners = new ArrayList<BusinessRole>();
			List<BusinessRole> providers = new ArrayList<BusinessRole>();
			List<BusinessRole> authors = new ArrayList<BusinessRole>();

			for (BusinessRole bRole : s.getBusinessRole()) {
				if (bRole.getType().equalsIgnoreCase(BusinessRole.roleType.OWNER.toString())) {
					owners.add(bRole);
				} else if (bRole.getType().equalsIgnoreCase(BusinessRole.roleType.PROVIDER.toString())) {
					providers.add(bRole);
				} else if (bRole.getType().equalsIgnoreCase(BusinessRole.roleType.AUTHOR.toString())) {
					authors.add(bRole);
				}
			}

			// add owner to rsd.
			for (BusinessRole owner : owners) {
				Resource r = m.createResource(buildingBlock.getURI() + owner.getTitle().trim().replaceAll(" ", "-"),
						WeliveCore.Entity).addProperty(DCTerms.title, owner.getTitle().trim());
				r.addProperty(WeliveCore.businessRole, ExtWeliveCore.Owner);
				r.addProperty(Foaf.page, owner.getWebpage());
				if (owner.getEmail() != null)
					r.addProperty(Foaf.mbox, owner.getEmail());
				buildingBlock.addProperty(WeliveCore.hasBusinessRole, r);
			}

			// add provider to rsd.
			for (BusinessRole provider : providers) {
				Resource r = m
						.createResource(buildingBlock.getURI() + provider.getTitle().trim().replaceAll(" ", "-"),
								WeliveCore.Entity)
						.addProperty(DCTerms.title, provider.getTitle()).addProperty(Foaf.page, provider.getWebpage());
				r.addProperty(WeliveCore.businessRole, ExtWeliveCore.Provider);
				if (provider.getEmail() != null)
					r.addProperty(Foaf.mbox, provider.getEmail());
				buildingBlock.addProperty(WeliveCore.hasBusinessRole, r);
			}

			for (BusinessRole author : authors) {
				Resource r = m
						.createResource(buildingBlock.getURI() + author.getTitle().trim().replaceAll(" ", "-"),
								WeliveCore.Entity)
						.addProperty(DCTerms.title, author.getTitle()).addProperty(Foaf.page, author.getWebpage());
				r.addProperty(WeliveCore.businessRole, ExtWeliveCore.Author);
				if (author.getEmail() != null)
					r.addProperty(Foaf.mbox, author.getEmail());
				buildingBlock.addProperty(WeliveCore.hasBusinessRole, r);
			}
		}

		// license
		if (!StringUtils.isBlank(s.getLicense().getTitle())) {

			Resource r = null;

			if (s.getLicense().getType().equalsIgnoreCase(LegalCondition.type.STANDARD.toString())) {
				r = m.createResource(buildingBlock.getURI() + "license", WeliveCore.StandardLicense)
						.addProperty(DCTerms.title, s.getLicense().getTitle());
			} else {
				r = m.createResource(buildingBlock.getURI() + "license", WeliveCore.CustomLicense)
						.addProperty(DCTerms.title, s.getLicense().getTitle());
			}

			buildingBlock.addProperty(WeliveCore.hasLegalCondition, r);
		}

		// interaction point.
		for (InteractionPoint ip : s.getInteractionPoint()) {
			Resource protocol = null;
			Resource r = null;

			if (ip.getProtocol().equalsIgnoreCase(InteractionPoint.protocol.REST.toString())) {
				protocol = WeliveCore.RESTWebServiceIP;

			} else if (ip.getProtocol().equalsIgnoreCase(InteractionPoint.protocol.SOAP.toString())) {
				protocol = WeliveCore.SOAPWebServiceIP;
			}
			r = m.createResource(buildingBlock.getURI() + "endpoint", protocol).addProperty(schemaUrlProp, ip.getUrl());
			if (ip.getTitle() != null)
				r.addProperty(DCTerms.title, ip.getTitle());
			if (ip.getUrl() != null)
				r.addProperty(WeliveCore.wadl, ip.getUrl() + "/service/spec/xwadl");

			buildingBlock.addProperty(WeliveCore.hasInteractionPoint, r);
		}

		if (s.getSecurity() != null) {

			// communication measure.
			if (s.getSecurity().getCommunication() != null) {
				Resource r = m.createResource(buildingBlock.getURI() + "Communication",
						WeliveSecurity.CommunicationMeasure);

				for (String origin : s.getSecurity().getCommunication().getOrigin()) {
					r.addProperty(WeliveSecurity.origin, origin);
				}
				for (String protocol : s.getSecurity().getCommunication().getProtocol()) {
					r.addProperty(WeliveSecurity.protocol, protocol);
				}
				buildingBlock.addProperty(WeliveSecurity.hasSecurityMeasure, r);
			}

			if (s.getSecurity().getAuthentication() != null) {

				Resource r = m.createResource(buildingBlock.getURI() + "Authentication",
						WeliveSecurity.AuthenticationMeasure);

				if (s.getSecurity().getAuthentication().getTitle() != null)
					r.addProperty(DCTerms.title, s.getSecurity().getAuthentication().getTitle());
				if (s.getSecurity().getAuthentication().getDescription() != null)
					r.addProperty(DCTerms.description, s.getSecurity().getAuthentication().getDescription());

				if (s.getSecurity().getAuthentication().getAccessType() != null) {
					r.addProperty(WeliveSecurity.accessType, s.getSecurity().getAuthentication().getAccessType());
				}
				for (String protocolType : s.getSecurity().getAuthentication().getProtocol()) {
					r.addProperty(WeliveSecurity.protocolType, protocolType);
				}

				for (String idp : s.getSecurity().getAuthentication().getIdentityProvider()) {
					r.addProperty(WeliveSecurity.withIdentityProvider, idp);
				}

				buildingBlock.addProperty(WeliveSecurity.hasSecurityMeasure, r);
			}

			if (s.getSecurity().getAuthorization() != null) {
				Resource r = m.createResource(buildingBlock.getURI() + "Authorization",
						WeliveSecurity.AuthorizationMeasure);
				for (Permission p : s.getSecurity().getAuthorization().getPermission()) {
					if (p.getTitle() != null)
						r.addProperty(DCTerms.title, p.getTitle());
					if (p.getIdentifier() != null)
						r.addProperty(DCTerms.identifier, p.getIdentifier());
					if (p.getDescription() != null)
						r.addProperty(DCTerms.description, p.getDescription());
					r.addProperty(WeliveSecurity.requires, r);
				}
				buildingBlock.addProperty(WeliveSecurity.hasSecurityMeasure, r);
			}

		}

		StringWriter sw = new StringWriter();
		m.write(sw);
		return sw.toString();
	}

}
