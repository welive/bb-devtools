/**
 * Copyright 2017 Smart Community Lab
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.smartcommunitylab.usdl.generator.model;

import java.util.List;

public class AuthenticationMeasure {

	/** list of identity providers (1:n). <:withIdentityProvider>**/
	private List<String> identityProvider;
	/** accessType. <:accessType> **/
	private String accessType;
	/** protocol type. <:protocolType>**/
	private List<String> protocol;
	/** title. **/
	private String title;
	/** description. **/
	private String description;

	public AuthenticationMeasure() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<String> getIdentityProvider() {
		return identityProvider;
	}

	public void setIdentityProvider(List<String> identityProvider) {
		this.identityProvider = identityProvider;
	}

	public String getAccessType() {
		return accessType;
	}

	public void setAccessType(String accessType) {
		this.accessType = accessType;
	}

	public List<String> getProtocol() {
		return protocol;
	}

	public void setProtocol(List<String> protocol) {
		this.protocol = protocol;
	}

}