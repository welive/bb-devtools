/**
 * Copyright 2017 Smart Community Lab
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.smartcommunitylab.usdl.generator.schema;

import it.smartcommunitylab.usdl.generator.schema.WeliveCore;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Resource;

public class ExtWeliveCore extends WeliveCore {
	private static Model m_model = ModelFactory.createDefaultModel();

	public static final Resource Owner = m_model.createResource(NS + "Owner");
	public static final Resource Provider = m_model.createResource(NS + "Provider");
	public static final Resource Author = m_model.createResource(NS + "Author");
	
	public static final Resource WebService = m_model.createResource(NS + "WebService");

}