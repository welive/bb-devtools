/**
 * Copyright 2017 Smart Community Lab
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.smartcommunitylab.usdl.generator.model;

public class TechnicalConstraint {
	private String language;
	private String body;
	private String type;

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	private enum type {
		PRECONDITION {
			public String toString() {
				return "pre-condition";
			}
		},
		POSTCONDITION {
			public String toString() {
				return "post-condition";
			}
		},
		MONITORING {
			public String toString() {
				return "monitoring";
			}
		}
	};

}
