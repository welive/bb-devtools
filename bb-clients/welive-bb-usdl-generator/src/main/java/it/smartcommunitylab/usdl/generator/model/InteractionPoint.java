/**
 * Copyright 2017 Smart Community Lab
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.smartcommunitylab.usdl.generator.model;

import java.util.List;

public class InteractionPoint {
	/** title. **/
	private String title;
	/** protocol. **/
	private String protocol;
	/** description. **/
	private String description;
	/** url. **/
	private String url;
	/** metadata. **/
	private String metadata;
	/** wadl. **/
	private String wadl;
	/** wsdl. **/
	private String wsdl;
	/** technical constraints. (1:n) **/
	private List<TechnicalConstraint> technicalConstraints;

	public InteractionPoint() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getMetadata() {
		return metadata;
	}

	public void setMetadata(String metadata) {
		this.metadata = metadata;
	}

	public String getWadl() {
		return wadl;
	}

	public void setWadl(String wadl) {
		this.wadl = wadl;
	}

	public String getWsdl() {
		return wsdl;
	}

	public void setWsdl(String wsdl) {
		this.wsdl = wsdl;
	}

	public List<TechnicalConstraint> getTechnicalConstraints() {
		return technicalConstraints;
	}

	public void setTechnicalConstraints(List<TechnicalConstraint> technicalConstraints) {
		this.technicalConstraints = technicalConstraints;
	}

	public enum protocol {
		REST {
			public String toString() {
				return "rest";
			}
		},
		SOAP {
			public String toString() {
				return "soap";
			}
		}
	}
}
