/**
 * Copyright 2017 Smart Community Lab
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.smartcommunitylab.usdl.generator.model;

public class Artifact {

	private BuildingBlock buildingblock;
	private PublicServiceApplication PSA;
	private Dataset DS;

	public BuildingBlock getBuildingblock() {
		return buildingblock;
	}

	public Artifact() {
		super();
		// TODO Auto-generated constructor stub
	}

	public void setBuildingblock(BuildingBlock buildingblock) {
		this.buildingblock = buildingblock;
	}

	public PublicServiceApplication getPSA() {
		return PSA;
	}

	public void setPSA(PublicServiceApplication pSA) {
		PSA = pSA;
	}

	public Dataset getDS() {
		return DS;
	}

	public void setDS(Dataset dS) {
		DS = dS;
	}

}
