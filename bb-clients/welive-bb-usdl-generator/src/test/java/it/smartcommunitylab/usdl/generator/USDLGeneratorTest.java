/**
 * Copyright 2017 Smart Community Lab
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.smartcommunitylab.usdl.generator;

import java.io.File;
import java.text.ParseException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class USDLGeneratorTest {

	USDLService usdlService;

	@Before
	public void init() {
		File f = new File("src/test/resources/artifact-reference.yml");
		usdlService = new USDLService(f);
	}

	@Test
	public void paresYAMLUsingFile() {
		Assert.assertTrue(usdlService.getArtifact().getBuildingblock() != null);
	}

	@Test
	public void generateRDF() throws ParseException {

		Assert.assertTrue(usdlService.generateBB().startsWith("<rdf:RDF"));
	}

}
