/**
 * Copyright 2017 Smart Community Lab
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.trentorise.smartcampus.aac.services;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import it.smartcommunitylab.welive.aac.client.exception.WeliveException;
import it.smartcommunitylab.welive.aac.client.model.BasicClientInfo;
import it.smartcommunitylab.welive.aac.client.model.ClientModel;
import it.smartcommunitylab.welive.aac.client.model.PermissionData;
import it.smartcommunitylab.welive.aac.client.service.ResourceService;
import junit.framework.Assert;

public class TestResourceService {

	private ResourceService resourceService;
	private final String scope = "profile.basicprofile.me";

	@Before
	public void init() {
		resourceService = new ResourceService(Constants.SERVER_URL);
	}

	@Test
	public void canAccessRescource() throws WeliveException {

		Boolean canAccess = resourceService.canAccessResource(Constants.USER_AUTH_TOKEN, scope);
		Assert.assertTrue(canAccess);
		System.out.println(canAccess);

	}

	@Test
	public void getClientInfo() throws WeliveException {

		BasicClientInfo basicClientInfo = resourceService.getBasicClientInfo(Constants.USER_AUTH_TOKEN);
		Assert.assertNotNull(basicClientInfo);
	}

	@Test
	public void getClientsInfo() throws WeliveException {

		List<BasicClientInfo> basicClientsInfo = resourceService.getBasicClientsInfo(Constants.USER_AUTH_TOKEN);
		Assert.assertFalse(basicClientsInfo.isEmpty());
		System.out.println(basicClientsInfo);
	}

	@Test
	public void getServicePermissions() throws WeliveException {
		PermissionData permissions = resourceService.getServicePermissions();
		Assert.assertNotNull(permissions);
	
	}

	@Test
	public void CRUDClientSpecificationForUser() throws WeliveException {
		
		// read client spec.
		ClientModel model = resourceService.getClientSpec(Constants.USER_AUTH_TOKEN);
		// create
		model.setName("delete" + System.currentTimeMillis());
		model.getGrantedTypes().clear();
		model.getIdentityProviders().clear();
		model.getOwnParameters().clear();
		model.getScopes().clear();
		model.setSloUrl("http://localhost/slo");
		model = resourceService.createClientSpec(Constants.USER_AUTH_TOKEN, model);
		Assert.assertNotNull(model);
	
	}

}
