/**
 * Copyright 2017 Smart Community Lab
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.trentorise.smartcampus.aac.services;
///*******************************************************************************
// * Copyright 2012-2013 Trento RISE
// * 
// *    Licensed under the Apache License, Version 2.0 (the "License");
// *    you may not use this file except in compliance with the License.
// *    You may obtain a copy of the License at
// * 
// *        http://www.apache.org/licenses/LICENSE-2.0
// * 
// *    Unless required by applicable law or agreed to in writing, software
// *    distributed under the License is distributed on an "AS IS" BASIS,
// *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// *    See the License for the specific language governing permissions and
// *    limitations under the License.
// ******************************************************************************/
//package eu.trentorise.smartcampus.profileservice;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import org.junit.Before;
//import org.junit.Test;
//
//import it.smartcommunitylab.welive.aac.client.exception.WeliveException;
//import it.smartcommunitylab.welive.aac.client.model.AccountProfile;
//import it.smartcommunitylab.welive.aac.client.model.BasicProfile;
//import it.smartcommunitylab.welive.aac.client.model.ExtUser;
//import it.smartcommunitylab.welive.aac.client.service.ProfileService;
//import it.smartcommunitylab.welive.aac.client.service.UserService;
//import junit.framework.Assert;
//
//public class TestUserService {
//
//	private UserService userService;
//	private ProfileService profileService;
//	private static final String BASICAUTHTOKEN = "Basic d2VsaXZlQHdlbGl2ZS5ldTp3M2wxdjN0MDBscw==";
//	private ExtUser extUser = new ExtUser("TestUserService", "TestUserService", "", "testuser@testuserservice.test");
//
//	@Before
//	public void init() {
//		userService = new UserService(Constants.SERVER_URL);
//		profileService = new ProfileService(Constants.SERVER_URL);
//	}
//
//	@Test
//	public void createDeleteUserTest() throws SecurityException, WeliveException {
//
//		String userId = userService.createUser(BASICAUTHTOKEN, extUser);
//		Assert.assertTrue(userId.matches("^-?\\d+$"));
//		System.out.println(userId);
//		
//		// check account profile.
//		BasicProfile result = profileService.getBasicProfileByUserId(userId,Constants.USER_AUTH_TOKEN);
//		Assert.assertNotNull(result);
//		List<String> userIds = new ArrayList<String>();
//		userIds.add(userId);
//		List<AccountProfile> result2 = profileService.getAccountProfilesByUserId(userIds, Constants.CLIENT_AUTH_TOKEN);
//		Assert.assertFalse(result2.isEmpty());
//	
//		int response = userService.deleteUsingUserId(BASICAUTHTOKEN, userId);
//		Assert.assertEquals(response, 200);
//
//	}
//
//}
