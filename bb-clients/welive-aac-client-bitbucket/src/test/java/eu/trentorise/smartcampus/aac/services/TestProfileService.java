/**
 * Copyright 2017 Smart Community Lab
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.trentorise.smartcampus.aac.services;

import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import it.smartcommunitylab.welive.aac.client.exception.WeliveException;
import it.smartcommunitylab.welive.aac.client.model.AccountProfile;
import it.smartcommunitylab.welive.aac.client.model.BasicProfile;
import it.smartcommunitylab.welive.aac.client.service.ProfileService;
import junit.framework.Assert;

public class TestProfileService {

	private ProfileService profileConnector;

	@Before
	public void init() {
		profileConnector = new ProfileService(Constants.SERVER_URL);
	}

	@Test
	public void accountProfile() throws SecurityException, WeliveException {
		AccountProfile accountProfile = profileConnector.getAccountProfile(Constants.USER_AUTH_TOKEN);
		Assert.assertNotNull(accountProfile);
		System.out.println(accountProfile);
		
		BasicProfile bp = profileConnector.getBasicProfile(Constants.USER_AUTH_TOKEN);
		List<AccountProfile> profiles = profileConnector.getAccountProfilesByUserId(Collections.singletonList(bp.getUserId()), Constants.CLIENT_AUTH_TOKEN);
		System.err.println(profiles);
	}
	@Test
	public void basicProfile() throws Exception {
		BasicProfile result;
		List<BasicProfile> results;

		// get token owner profile
		result = profileConnector.getBasicProfile(Constants.USER_AUTH_TOKEN);
		Assert.assertNotNull(result);
		System.out.println(result);

		// get token owner profile
		result = profileConnector.getBasicProfileByUserId(result.getUserId(),Constants.USER_AUTH_TOKEN);
		Assert.assertNotNull(result);
		System.out.println(result);

		// get all profiles
		results = profileConnector.getBasicProfiles(null,
				Constants.USER_AUTH_TOKEN);
		Assert.assertNotNull(results);
		System.out.println(results);

		// get profiles with filter
		results = profileConnector
				.getBasicProfiles("a", Constants.USER_AUTH_TOKEN);
		Assert.assertNotNull(results);
		System.out.println(results);

		// get profiles with filter, no results
		results = profileConnector
				.getBasicProfiles("=", Constants.USER_AUTH_TOKEN);
		Assert.assertTrue(results == null || results.size() == 0);
		System.out.println(results);
	}

}
