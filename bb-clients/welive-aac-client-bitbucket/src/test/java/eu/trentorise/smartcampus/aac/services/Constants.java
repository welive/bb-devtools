/**
 * Copyright 2017 Smart Community Lab
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.trentorise.smartcampus.aac.services;

/**
 * 
 * @author nawazk
 *
 */
public class Constants {

	static final String USER_AUTH_TOKEN = "e9448a26-df18-4644-b799-84f57453f70f";
	static final String CLIENT_AUTH_TOKEN = "e22ed6d4-f86d-4ad1-9e74-88fd9cc4e6b5";
	static final String USER_ID = "494";
	static final String SERVER_URL = "https://test.welive.eu/dev/api/aac";
	static final String DEFAULT_LANGUAGE_CODE = "en";
	
}
