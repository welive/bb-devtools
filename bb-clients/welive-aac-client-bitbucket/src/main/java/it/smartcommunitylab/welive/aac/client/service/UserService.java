/**
 * Copyright 2017 Smart Community Lab
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//package it.smartcommunitylab.welive.aac.client.service;
//
//import java.io.BufferedReader;
//import java.io.InputStream;
//import java.io.InputStreamReader;
//
//import org.apache.http.HttpResponse;
//import org.apache.http.HttpStatus;
//import org.apache.http.client.HttpClient;
//import org.apache.http.client.methods.HttpDelete;
//import org.apache.http.client.methods.HttpPost;
//import org.apache.http.conn.params.ConnManagerParams;
//import org.apache.http.entity.StringEntity;
//import org.apache.http.impl.client.DefaultHttpClient;
//import org.apache.http.params.HttpConnectionParams;
//import org.apache.http.params.HttpParams;
//import org.codehaus.jackson.map.ObjectMapper;
//
//import it.smartcommunitylab.welive.aac.client.exception.WeliveException;
//import it.smartcommunitylab.welive.aac.client.model.ExtUser;
//
//public class UserService {
//
//	private String userServiceURL;
//
//	/** user service path */
//	private static final String USER = "user/";
//
//	/** ext user path. */
//	private static final String EXTUSER = "extuser/";
//
//	/** Timeout (in ms) we specify for each http request */
//	public static final int HTTP_REQUEST_TIMEOUT_MS = 30 * 1000;
//
//	private static ObjectMapper mapper = new ObjectMapper();
//
//	public UserService(String serverURL) {
//		this.userServiceURL = serverURL;
//		if (!userServiceURL.endsWith("/"))
//			userServiceURL += '/';
//	}
//
//	/**
//	 * Create user.
//	 * @param token
//	 * @return
//	 * @throws SecurityException
//	 * @throws WeliveException
//	 */
//	public String createUser(String token, ExtUser extUser) throws WeliveException {
//		try {
//			final HttpResponse resp;
//			String url = userServiceURL + EXTUSER + "create";
//			final HttpPost post = new HttpPost(url);
//
//			// body.
//			StringEntity se = new StringEntity(mapper.writeValueAsString(extUser));
//			post.setEntity(se);
//			post.setHeader("Content-Type", "application/json");
//			post.setHeader("Accept", "application/json");
//			post.setHeader("Authorization", token);
//			try {
//				resp = getHttpClient().execute(post);
//				InputStream ips = resp.getEntity().getContent();
//				BufferedReader buf = new BufferedReader(new InputStreamReader(ips, "UTF-8"));
//				if (resp.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
//					throw new WeliveException("Error validating " + resp.getStatusLine());
//				}
//				StringBuilder sb = new StringBuilder();
//				String s;
//				while (true) {
//					s = buf.readLine();
//					if (s == null || s.length() == 0)
//						break;
//					sb.append(s);
//
//				}
//				buf.close();
//				ips.close();
//				return sb.toString();
//
//			} catch (final Exception e) {
//				throw new WeliveException(e);
//			}
//		} catch (Exception e) {
//			throw new WeliveException(e);
//		}
//	}
//
//	/**
//	 * Delete user using OAuth
//	 * @param token
//	 * @return
//	 * @throws WeliveException
//	 */
//	public int deleteUser(String token) throws WeliveException {
//		try {
//			final HttpResponse resp;
//			String url = userServiceURL + USER + "me";
//			final HttpDelete delete = new HttpDelete(url);
//
//			delete.setHeader("Accept", "application/json");
//			delete.setHeader("Authorization", token);
//			try {
//				resp = getHttpClient().execute(delete);
//				if (resp.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
//					return resp.getStatusLine().getStatusCode();
//				}
//				throw new WeliveException("Error validating " + resp.getStatusLine());
//			} catch (final Exception e) {
//				throw new WeliveException(e);
//			}
//		} catch (Exception e) {
//			throw new WeliveException(e);
//		}
//	}
//
//	/**
//	 * Delete using Ids.
//	 * @param token
//	 * @param userIds
//	 * @return
//	 * @throws WeliveException
//	 */
//	public int deleteUsingUserId(String token, String userIds) throws WeliveException {
//		try {
//			final HttpResponse resp;
//			String url = userServiceURL + USER + userIds + "?cascade=true";
//			final HttpDelete delete = new HttpDelete(url);
//
//			delete.setHeader("Accept", "application/json");
//			delete.setHeader("Authorization", token);
//			try {
//				resp = getHttpClient().execute(delete);
//				if (resp.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
//					return resp.getStatusLine().getStatusCode();
//				}
//				throw new WeliveException("Error validating " + resp.getStatusLine());
//			} catch (final Exception e) {
//				throw new WeliveException(e);
//			}
//		} catch (Exception e) {
//			throw new WeliveException(e);
//		}
//	}
//
//	protected static HttpClient getHttpClient() {
//		HttpClient httpClient = new DefaultHttpClient();
//		final HttpParams params = httpClient.getParams();
//		HttpConnectionParams.setConnectionTimeout(params, HTTP_REQUEST_TIMEOUT_MS);
//		HttpConnectionParams.setSoTimeout(params, HTTP_REQUEST_TIMEOUT_MS);
//		ConnManagerParams.setTimeout(params, HTTP_REQUEST_TIMEOUT_MS);
//		return httpClient;
//	}
//}
