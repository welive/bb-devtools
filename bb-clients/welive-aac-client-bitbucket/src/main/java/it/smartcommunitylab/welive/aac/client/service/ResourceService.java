/**
 * Copyright 2017 Smart Community Lab
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.smartcommunitylab.welive.aac.client.service;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.codehaus.jackson.map.ObjectMapper;

import eu.trentorise.smartcampus.network.JsonUtils;
import it.smartcommunitylab.welive.aac.client.exception.WeliveException;
import it.smartcommunitylab.welive.aac.client.model.BasicClientInfo;
import it.smartcommunitylab.welive.aac.client.model.ClientModel;
import it.smartcommunitylab.welive.aac.client.model.PermissionData;

public class ResourceService {

	private String resourceServiceUrl;

	/** user service path */
	private static final String RESOURCE = "resources/";

	/** Timeout (in ms) we specify for each http request */
	public static final int HTTP_REQUEST_TIMEOUT_MS = 30 * 1000;

	private static ObjectMapper mapper = new ObjectMapper();

	public ResourceService(String serverURL) {
		this.resourceServiceUrl = serverURL;
		if (!resourceServiceUrl.endsWith("/"))
			resourceServiceUrl += '/';
	}

	/**
	 * Create user.
	 * @param token
	 * @return
	 * @throws SecurityException
	 * @throws WeliveException
	 */
	public Boolean canAccessResource(String token, String scope) throws WeliveException {
		try {
			final HttpResponse resp;
			String url = resourceServiceUrl + RESOURCE + "access?scope=" + scope;
			final HttpGet get = new HttpGet(url);

			get.setHeader("Content-Type", "application/json");
			get.setHeader("Accept", "application/json");
			get.setHeader("Authorization", "Bearer " + token);
			try {
				resp = getHttpClient().execute(get);
				InputStream ips = resp.getEntity().getContent();
				BufferedReader buf = new BufferedReader(new InputStreamReader(ips, "UTF-8"));
				if (resp.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
					throw new WeliveException("Error validating " + resp.getStatusLine());
				}
				StringBuilder sb = new StringBuilder();
				String s;
				while (true) {
					s = buf.readLine();
					if (s == null || s.length() == 0)
						break;
					sb.append(s);

				}
				buf.close();
				ips.close();
				return Boolean.valueOf(sb.toString());

			} catch (final Exception e) {
				throw new WeliveException(e);
			}
		} catch (Exception e) {
			throw new WeliveException(e);
		}
	}

	/**
	 * Get information about the OAuth client app making the request, namely client app ID and client app name.
	 * @param token
	 * @return
	 * @throws WeliveException
	 */
	public BasicClientInfo getBasicClientInfo(String token) throws WeliveException {

		try {
			final HttpResponse resp;
			String url = resourceServiceUrl + RESOURCE + "clientinfo";
			final HttpGet get = new HttpGet(url);

			get.setHeader("Content-Type", "application/json");
			get.setHeader("Accept", "application/json");
			get.setHeader("Authorization", "Bearer " + token);
			try {
				resp = getHttpClient().execute(get);
				InputStream ips = resp.getEntity().getContent();
				BufferedReader buf = new BufferedReader(new InputStreamReader(ips, "UTF-8"));
				if (resp.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
					throw new WeliveException("Error validating " + resp.getStatusLine());
				}
				StringBuilder sb = new StringBuilder();
				String s;
				while (true) {
					s = buf.readLine();
					if (s == null || s.length() == 0)
						break;
					sb.append(s);

				}
				buf.close();
				ips.close();
				return JsonUtils.toObject(sb.toString(), BasicClientInfo.class);

			} catch (final Exception e) {
				throw new WeliveException(e);
			}
		} catch (Exception e) {
			throw new WeliveException(e);
		}
	}

	/**
	 * Get information about the all OAuth clients making the request, namely client app ID and client app name.
	 * @param token
	 * @return
	 * @throws WeliveException
	 */
	public List<BasicClientInfo> getBasicClientsInfo(String token) throws WeliveException {
		try {
			final HttpResponse resp;
			String url = resourceServiceUrl + RESOURCE + "clientinfo/oauth";
			final HttpGet get = new HttpGet(url);

			get.setHeader("Content-Type", "application/json");
			get.setHeader("Accept", "application/json");
			get.setHeader("Authorization", "Bearer " + token);
			try {
				resp = getHttpClient().execute(get);
				InputStream ips = resp.getEntity().getContent();
				BufferedReader buf = new BufferedReader(new InputStreamReader(ips, "UTF-8"));
				if (resp.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
					throw new WeliveException("Error validating " + resp.getStatusLine());
				}
				StringBuilder sb = new StringBuilder();
				String s;
				while (true) {
					s = buf.readLine();
					if (s == null || s.length() == 0)
						break;
					sb.append(s);

				}
				buf.close();
				ips.close();

				return ((List<BasicClientInfo>) JsonUtils.toObject(sb.toString(), List.class));

			} catch (final Exception e) {
				throw new WeliveException(e);
			}
		} catch (Exception e) {
			throw new WeliveException(e);
		}
	}

	/**
	 * Read Service Permissions.
	 * @return
	 * @throws WeliveException
	 */
	public PermissionData getServicePermissions() throws WeliveException {
		try {
			final HttpResponse resp;
			String url = resourceServiceUrl + RESOURCE + "permissions";
			final HttpGet get = new HttpGet(url);

			get.setHeader("Content-Type", "application/json");
			get.setHeader("Accept", "application/json");
			try {
				resp = getHttpClient().execute(get);
				InputStream ips = resp.getEntity().getContent();
				BufferedReader buf = new BufferedReader(new InputStreamReader(ips, "UTF-8"));
				if (resp.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
					throw new WeliveException("Error validating " + resp.getStatusLine());
				}
				StringBuilder sb = new StringBuilder();
				String s;
				while (true) {
					s = buf.readLine();
					if (s == null || s.length() == 0)
						break;
					sb.append(s);

				}
				buf.close();
				ips.close();

				return JsonUtils.toObject(sb.toString(), PermissionData.class);

			} catch (final Exception e) {
				throw new WeliveException(e);
			}
		} catch (Exception e) {
			throw new WeliveException(e);
		}
	}

	/**
	 * Read client specification for user.
	 * @param token
	 * @return
	 * @throws WeliveException
	 */
	public ClientModel getClientSpec(String token) throws WeliveException {
		try {
			final HttpResponse resp;
			String url = resourceServiceUrl + RESOURCE + "clientspec";
			final HttpGet get = new HttpGet(url);

			get.setHeader("Content-Type", "application/json");
			get.setHeader("Accept", "application/json");
			get.setHeader("Authorization", "Bearer " + token);
			try {
				resp = getHttpClient().execute(get);
				InputStream ips = resp.getEntity().getContent();
				BufferedReader buf = new BufferedReader(new InputStreamReader(ips, "UTF-8"));
				if (resp.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
					throw new WeliveException("Error validating " + resp.getStatusLine());
				}
				StringBuilder sb = new StringBuilder();
				String s;
				while (true) {
					s = buf.readLine();
					if (s == null || s.length() == 0)
						break;
					sb.append(s);

				}
				buf.close();
				ips.close();

				return JsonUtils.toObject(sb.toString(), ClientModel.class);

			} catch (final Exception e) {
				throw new WeliveException(e);
			}
		} catch (Exception e) {
			throw new WeliveException(e);
		}
	}

	/**
	 * Create client specification for user.
	 * @param token
	 * @param model
	 * @return
	 * @throws WeliveException
	 */
	public ClientModel createClientSpec(String token, ClientModel model) throws WeliveException {
		try {
			final HttpResponse resp;
			String url = resourceServiceUrl + RESOURCE + "clientspec";
			final HttpPost post = new HttpPost(url);

			// body.
			StringEntity se = new StringEntity(mapper.writeValueAsString(model));
			post.setEntity(se);
			post.setHeader("Content-Type", "application/json");
			post.setHeader("Accept", "application/json");
			post.setHeader("Authorization", "Bearer " + token);
			try {
				resp = getHttpClient().execute(post);
				InputStream ips = resp.getEntity().getContent();
				BufferedReader buf = new BufferedReader(new InputStreamReader(ips, "UTF-8"));
				if (resp.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
					throw new WeliveException("Error validating " + resp.getStatusLine());
				}
				StringBuilder sb = new StringBuilder();
				String s;
				while (true) {
					s = buf.readLine();
					if (s == null || s.length() == 0)
						break;
					sb.append(s);

				}
				buf.close();
				ips.close();
				return JsonUtils.toObject(sb.toString(), ClientModel.class);

			} catch (final Exception e) {
				throw new WeliveException(e);
			}
		} catch (Exception e) {
			throw new WeliveException(e);
		}
	}

	/**
	 * Update client specification for user.
	 * @param token
	 * @param model
	 * @return
	 * @throws WeliveException
	 */
	public ClientModel updateClientSpec(String token, ClientModel model) throws WeliveException {
		try {
			final HttpResponse resp;
			String url = resourceServiceUrl + RESOURCE + "clientspec";
			final HttpPut put = new HttpPut(url);

			// body.
			StringEntity se = new StringEntity(mapper.writeValueAsString(model));
			put.setEntity(se);
			put.setHeader("Content-Type", "application/json");
			put.setHeader("Accept", "application/json");
			put.setHeader("Authorization", "Bearer " + token);
			try {
				resp = getHttpClient().execute(put);
				InputStream ips = resp.getEntity().getContent();
				BufferedReader buf = new BufferedReader(new InputStreamReader(ips, "UTF-8"));
				if (resp.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
					throw new WeliveException("Error validating " + resp.getStatusLine());
				}
				StringBuilder sb = new StringBuilder();
				String s;
				while (true) {
					s = buf.readLine();
					if (s == null || s.length() == 0)
						break;
					sb.append(s);

				}
				buf.close();
				ips.close();
				return JsonUtils.toObject(sb.toString(), ClientModel.class);

			} catch (final Exception e) {
				throw new WeliveException(e);
			}
		} catch (Exception e) {
			throw new WeliveException(e);
		}
	}

	protected static HttpClient getHttpClient() {
		HttpClient httpClient = new DefaultHttpClient();
		final HttpParams params = httpClient.getParams();
		HttpConnectionParams.setConnectionTimeout(params, HTTP_REQUEST_TIMEOUT_MS);
		HttpConnectionParams.setSoTimeout(params, HTTP_REQUEST_TIMEOUT_MS);
		ConnManagerParams.setTimeout(params, HTTP_REQUEST_TIMEOUT_MS);
		return httpClient;
	}

}
